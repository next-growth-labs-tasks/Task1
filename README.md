# SECTION A - Making a simple website with vanilla JS

## Getting started
***

Click Here -> [Demo Link](https://next-growth-labs-tasks.gitlab.io/Task1/)

- Implemented all the relevant parameters. 

- Used input element in HTML and type range for the slider


- Implemented modal starter template from the BOOTSTRAP documentation and modified for the relavant input needs.

- On clicking any of the buttons a form template is displayed with `name`, `email` and `order comments`

- A function `sliderValueFunction` which calls every time there is change in the slider input and based on the value the relevant container is highlighted.

-  `box-shadow` is used to highlight the container based on the company size.

- Tried to implement the form submission but the given documentation to implement the API was POORLY WRITTEN and unable to figure out how to  use POST method.

- Optimised Core web vitals to meet the standard requirements for both desktop and mobile

- Hosted the website in the gitlab.io.

- For Continuos Deployement script is written as configaration file. Which automates deployement as soon as the changes are commited.